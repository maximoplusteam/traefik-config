const commits = [
  { commit: "cebb63e", title: "Start", id: "start" },
  { commit: "b4a580a", title: "Application Container", id: "appcontainer" },
  { commit: "aa9354e", title: "List Component", id: "listcomponent" },
  { commit: "bd2f918", title: "Section Component", id: "section" },
  { commit: "a880f09", title: "Search Component", id: "qbesection" },
  { commit: "2eadbf0", title: "Lookups", id: "lookups" },
  { commit: "5033d7e", title: "Search Lookups", id: "qbelookups" },
  {
    commit: "9fab064",
    title: "Basic Smartphone features: Make a phone call from your app",
    id: "phonecall"
  },
  { commit: "2fdca00", title: "Access the phone camera", id: "phonecamera" }
];

module.exports = { commits };
