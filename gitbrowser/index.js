const simpleGit = require("simple-git/promise");
const fs = require("fs");
const path = require("path");
const chmodr = require("chmodr");
const { commits } = require("./commits");
const isGitDirty = require("is-git-dirty");
const {
  setAccessTiming,
  getExpiredIds,
  removeIds,
  getAllOccupiedIds
} = require("../redis");

const commitsWithContent = commits.map(({ commit, title, id }) => {
  const filePath = path.join(__dirname, "../markdown-comments/" + id + ".md");
  return { commit, title, id, content: fs.readFileSync(filePath, "utf-8") };
});

const goToCommit = async (rootSrc, reservedId, commit) => {
  const workDir = rootSrc + reservedId;
  await setAccessTiming("access-", reservedId);
  const git = simpleGit(workDir);
  return await git.checkout(commit);
};

const getCommits = async () => {
  //this should list all the commits starting with the tag
  return commitsWithContent;
};

const checkoutUserBranch = async workDir => {
  //To enable editing the user branch, we will create the new branch for the user local changes, and then
  //change the file permissions
  const git = simpleGit(workDir);
  const localBranches = await git.branchLocal();
  //console.log(localBranches);
  const branchExists = localBranches.all.some(b => b === "user_changes");
  if (!branchExists) {
    await git.checkoutLocalBranch("user_changes");
  } else {
    await git.checkout("user_changes");
  }
  return new Promise((resolve, reject) => {
    chmodr(workDir, 0o777, err => {
      if (err) {
        console.log(err);

        reject(err);
      } else {
        //        console.log("chmod succ");
        resolve(workDir);
      }
    });
  });
};

const saveLocalUserBranchChanges = async (rootSrc, reservedId) => {
  //this will save the local changes to the directory. If the cache in the browser is deleted, user will lose this.
  //There will be an option to login with google account and save the changes
  const workDir = rootSrc + reservedId;
  if (isGitDirty(workDir)) {
    console.log("saving local changes to " + workDir);
    const git = simpleGit(workDir);
    const added = await git.add("./*");
    //  console.log(added);
    const commited = await git.commit("user commits changes from the tool");
    //console.log(commited);
    await setAccessTiming("change-", reservedId);
    await setAccessTiming("access-", reservedId);
    return commited;
  } else {
    console.log("No changes");
    return null;
  }
};

const showLogs = async workDir => {
  const git = simpleGit(workDir);
  return await git.log();
};

const lastLogDate = async workDir => {
  const logs = await showLogs(workDir);
  return logs.all[0].date;
};

const resetContainers = async (rootSrc, ids) => {
  if (!ids || ids.length === 0) {
    return ids;
  }
  for (const id of ids) {
    console.log("resetting the id:" + id);
    const workDir = rootSrc + id;
    const git = simpleGit(workDir);
    const { commit: firstCommit } = commits[0];
    await git.checkout(firstCommit);
    const localBranches = await git.branchLocal();
    const branchExists = localBranches.all.some(b => b === "user_changes");
    if (branchExists) {
      console.log("deleting the user changed branch " + id);
      await git.deleteLocalBranch("user_changes", true);
    }
  }
  return await removeIds(ids);
};

const removeExpired = async rootSrc => {
  const expiredIds = await getExpiredIds(5 * 60 * 1000);
  if (!expiredIds || expiredIds.length === 0) {
    return expiredIds;
  }
  return await resetContainers(rootSrc, expiredIds);
};

const commitDirty = async rootSrc => {
  const allOccupiedIds = await getAllOccupiedIds();
  const dirtyIds = allOccupiedIds.filter(id => {
    const isDirty = isGitDirty(rootSrc + id);
    return isDirty;
  });
  for (const d of dirtyIds) {
    await saveLocalUserBranchChanges(rootSrc, d);
  }
  return dirtyIds;
};

module.exports = {
  goToCommit,
  getCommits,
  checkoutUserBranch,
  saveLocalUserBranchChanges,
  showLogs,
  lastLogDate,
  removeExpired,
  commitDirty,
  resetContainers
};
