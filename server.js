const express = require("express");
const path = require("path");
const app = express();
const redis = require("redis");
const session = require("express-session");
const dotenv = require("dotenv-flow");
dotenv.config();
const { checkCaptcha } = require("./grecaptcha");
const { createServers } = require("./traefik");
const {
  reserveOne,
  releaseAllIds,
  getAllOccupiedIds,
  isValidId,
  subscribe,
  getFreeIds,
} = require("./redis");
const {
  goToCommit,
  getCommits,
  checkoutUserBranch,
  saveLocalUserBranchChanges,
  showLogs,
  commitDirty,
  removeExpired,
  resetContainers,
} = require("./gitbrowser");
let RedisStore = require("connect-redis")(session);

const redisHost = process.env.REDIS_HOST; //redisHost should be in Docker network, without exposing the port to the OS
const redisPort = 6379;
const randomSecret = process.env.RANDOM_SECRET;
const redisClient = redis.createClient(redisPort, redisHost);
const cors = require("cors");

const port = 3000;

const rootSrc = process.env.DEST_VOLUME + "/";

if (process.env.DEV) {
  app.use(cors({ origin: "http://localhost:3006", credentials: true }));
}
const store = new RedisStore({ client: redisClient });

app.use(
  session({
    store,
    secret: randomSecret,
    resave: false,
  })
);

app.set("trust proxy", true);

const adminMiddleware = (req, res, next) => {
  if (req.url.startsWith("/admin")) {
    if (req.session.authenticated) {
      next();
      return;
    }
    const auth = req.headers.authorization;

    if (!auth) {
      res.setHeader("WWW-Authenticate", 'Basic realm="Admin"');

      console.log("login failed. wrong username or password.");
      res.sendStatus(401);
      return;
    }
    var tmp = auth.split(" "); // Split on a space, the original auth looks like  "Basic Y2hhcmxlczoxMjM0NQ==" and we need the 2nd part

    var buf = Buffer.from(tmp[1], "base64"); // create a buffer and tell it the data coming in is base64
    var plain_auth = buf.toString(); // read it back out as a string
    var creds = plain_auth.split(":"); // split on a ':'
    var username = creds[0];
    var password = creds[1];

    if (username === "dusan" && password === "Stal02Ker2020#Maximo") {
      req.session.authenticated = true;
      next();
      return;
    }

    res.setHeader("WWW-Authenticate", 'Basic realm="Admin"');

    console.log("login failed. wrong username or password.");
    res.sendStatus(401);
    return;
  }
  next();
};

app.use(adminMiddleware);

app.use(express.static(path.join(__dirname, "client", "build")));

app.get("/checkcaptcha", async (req, res) => {
  try {
    const validCaptcha = await checkCaptcha(req.query.token, req.ip);
    if (!validCaptcha) {
      req.session.validCaptcha = false;
      res.status(500).send("Invalid captcha");
      return;
    }
    req.session.validCaptcha = true;
    res.setHeader("Content-Type", "application/json");
    res.write(JSON.stringify("ok"));
    res.end();
  } catch (err) {
    console.log(err);
    res.status(500).send(err);
  }
});

app.get("/admin/create/:noservers", (req, res) => {
  console.log("Creating new " + req.params.noservers);
  createServers(req.params.noservers)
    .then((servers) => {
      console.log(servers);
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify(servers));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
});

app.get("/admin/releaseall", (req, res) => {
  releaseAllIds()
    .then((servers) => {
      console.log(servers);
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify(servers));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
});

//think about how to restrict the call to this, or check the ip
app.get("/reservesr", (req, res) => {
  if (!req.session.validCaptcha) {
    res.status(500).send("Invalid captcha");
  }
  let reserved = "";
  if (req.session.reservedId) {
    reserved = new Promise((resolve, reject) => {
      return isValidId(req.session.reserveId).then((valid) => {
        if (valid) {
          resolve(req.session.reserveId);
        } else {
          console.log("session expired, getting a new one");
          reserveOne(null)
            .then((id) => {
              req.session.reservedId = id;
              resolve(id);
            })
            .catch((err) => {
              reject(err);
            });
        }
      });
    });
  } else {
    console.log("getting the new one:");
    console.log(req.query);
    reserved = reserveOne(req.query.trid).then((id) => {
      req.session.reservedId = id;
      return id;
    });
  }
  reserved
    .then((id) => {
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify(id));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res
        .status(500)
        .send(
          "All the emulators are occupied, please try again in couple of minutes"
        );
    });
});

app.get("/gitcheckout/:commit", (req, res) => {
  if (!req.session.reservedId) {
    res.status(500).send("Invalid Session");
    return;
  }
  isValidId(req.session.reservedId)
    .then((valid) => {
      //  console.log("session:" + req.session.reservedId);
      //  console.log(rootSrc + req.session.reservedId);
      if (!valid) {
        req.session.reservedId = null;

        return Promise.reject("Invalid Session");
      }
      return goToCommit(rootSrc, req.session.reservedId, req.params.commit);
    })
    .then((reslt) => {
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify(req.session.reservedId));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
});

app.get("/listcommits", (req, res) => {
  getCommits()
    .then((commits) => {
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify(commits));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
});

app.get("/checkoutuserbranch", (req, res) => {
  if (!req.session.reservedId) {
    res.status(500).send("Invalid Session");
    return;
  }
  checkoutUserBranch(rootSrc + req.session.reservedId)
    .then((ok) => {
      //      console.log(ok);
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify("ok"));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
});

app.get("/saveeditorlocal", (req, res) => {
  if (!req.session.reservedId) {
    res.status(500).send("Invalid Session");
    return;
  }
  saveLocalUserBranchChanges(rootSrc, req.session.reservedId)
    .then((ok) => {
      //  console.log(ok);
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify("ok"));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
});

app.get("/logs", (req, res) => {
  if (!req.session.reservedId) {
    res.status(500).send("Invalid Session");
    return;
  }
  showLogs(rootSrc + req.session.reservedId)
    .then((ok) => {
      //      console.log(ok);
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify(ok));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
});

app.get("/admin/occupiedids", (req, res) => {
  getAllOccupiedIds()
    .then((ids) => {
      return Promise.all(
        ids.map((id) => {
          return showLogs(rootSrc + id).then(({ latest: { hash, date } }) => {
            return { id, hash, date };
          });
        })
      );
    })
    .then((idcommits) => {
      res.setHeader("Content-Type", "application/json");
      res.write(JSON.stringify(idcommits));
      res.end();
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send(err);
    });
});

app.get("/sse", (req, res) => {
  res.set({
    "Cache-Control": "no-cache",
    "Content-Type": "text/event-stream",
    Connection: "keep-alive",
  });
  res.write("\n");
  subscribe(({ id, timeToExpiry }) => {
    if (req.session.reservedId == id) {
      if (timeToExpiry == 0) {
        console.log("Expiring the id " + id);
        delete req.session.reservedId;
        res.write("data: " + JSON.stringify({ event: "expiry" }) + "\n\n");
      } else {
        res.write(
          "data: " +
            JSON.stringify({ event: "reminder", time: timeToExpiry }) +
            "\n\n"
        );
      }
    }
  });
});

app.get("/admin/resetcontainers", async (req, res) => {
  try {
    const ids = JSON.parse(req.query.ids);
    console.log(ids);
    await resetContainers(rootSrc, ids);
    res.setHeader("Content-Type", "application/json");
    res.write(JSON.stringify(ids));
    res.end();
  } catch (err) {
    res.status(500).send(err);
  }
});

app.get("/admin/freeids", async (req, res) => {
  try {
    const ids = await getFreeIds();
    res.setHeader("Content-Type", "application/json");
    res.write(JSON.stringify(ids));
    res.end();
  } catch (err) {
    res.status(500).send(err);
  }
});

app.get("*", (req, res) => {
  res.sendFile("index.html", { root: path.join(__dirname, "client", "build") });
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

//commit dirty loop
setInterval(() => {
  commitDirty(rootSrc)
    .then((res) => {})
    .catch((err) => {
      console.log("error when commit dirty");
      console.log(err);
    });
}, 10000);

setInterval(() => {
  removeExpired(rootSrc)
    .then((res) => {})
    .catch((err) => console.log(err));
}, 20000);
//delete unused loop

subscribe((d) => {
  console.log("got from pubsub");
  console.log(d);
});
