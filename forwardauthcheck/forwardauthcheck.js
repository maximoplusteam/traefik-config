const express = require("express");
const app = express();
const redis = require("redis");
const session = require("express-session");

let RedisStore = require("connect-redis")(session);
const redisHost = "127.0.0.1"; //redisHost should be in Docker network, without exposing the port to the OS
const redisPort = 6379;
const randomSecret = "99i4CKlVwyoIxFGM0Os4";
const redisClient = redis.createClient(redisPort, redisHost);

const port = 3000;

app.use(
  session({
    store: new RedisStore({ client: redisClient }),
    secret: randomSecret,
    cookie: { maxAge: 60000 },
    resave: false
  })
);

app.get("/", (req, res) => {
  console.log(req.ip);
  console.log(req.headers);
  res.send("Your address is " + req.ip);
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
