FROM node:14.7.0
WORKDIR /app
COPY . .
WORKDIR /app/client
RUN yarn install
RUN yarn build
WORKDIR /app
RUN npm install
VOLUME /rndemo/volumes
CMD NODE_ENV=production node server
EXPOSE 3000
