import React, { useState, useEffect, useRef } from "react";
import "mini.css";
import axios from "axios";
axios.defaults.withCredentials = true;

export default () => {
  const [resultLabel, setResultLabel] = useState("");
  const [results, setResults] = useState([]);
  var displayRow = useRef(x => x);
  const reserveRef = useRef(null);
  const noIds = useRef(null);
  const checkOccupiedIds = () => {
    displayRow.current = ({ id, date }) => (
      <div>
        id:
        {id}, date:
        {date}
      </div>
    );
    axios
      .get(process.env.REACT_APP_SERVER_PATH + "/admin/occupiedIds")
      .then(({ data }) => {
        setResults(data);
        setResultLabel("Occupied IDS");
      });
  };

  const freeIds = ids => {
    displayRow.current = x => <div>{x}</div>;
    const idsArray = JSON.stringify(ids.split(",").map(x => x.trim()));
    axios
      .get(process.env.REACT_APP_SERVER_PATH + "/admin/resetcontainers", {
        params: { ids: idsArray }
      })
      .then(({ data }) => {
        setResults(data);
        setResultLabel("Freed IDS");
      });
  };

  const reserveIds = noIds => {
    displayRow.current = x => <div>{x}</div>;
    setResultLabel("Working, dont press button again!");
    axios
      .get(process.env.REACT_APP_SERVER_PATH + "/admin/create/" + noIds)
      .then(({ data }) => {
        setResults(data);
        setResultLabel("New IDS");
      });
  };

  const getFreeIds = () => {
    displayRow.current = x => <div>{x}</div>;
    axios
      .get(process.env.REACT_APP_SERVER_PATH + "/admin/freeids")
      .then(({ data }) => {
        setResults(data);
        setResultLabel("Free ids");
      });
  };

  return (
    <div>
      <div className="input-group">
        <label htmlFor="username">Reserve ids</label>
        <input
          ref={noIds}
          type="text"
          id="reserve"
          placeholder="No. of ids to reserve"
        />
        <button
          onClick={_ => {
            reserveIds(noIds.current.value);
          }}
        >
          Reserve
        </button>
      </div>
      <div />
      <div className="input-group">
        <label>See occupied</label>
        <button onClick={checkOccupiedIds}>Check</button>
      </div>
      <div />
      <div className="input-group">
        <label htmlFor="freeids">Free the ids</label>
        <input
          type="text"
          id="freeids"
          placeholder="Comma separated ids to free"
          ref={reserveRef}
        />
        <button
          onClick={_ => {
            console.log(reserveRef.current.value);
            freeIds(reserveRef.current.value);
          }}
        >
          Free
        </button>
      </div>
      <div />
      <div className="input-group">
        <label>View free ids</label>
        <button onClick={getFreeIds}>Check</button>
      </div>
      <header>{resultLabel}</header>
      <div className="card fluid">
        {results.map(r => {
          console.log(r);
          return displayRow.current(r);
        })}
      </div>
    </div>
  );
};
