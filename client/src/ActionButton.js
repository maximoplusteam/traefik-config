import React from "react";
import "mini.css";

export default ({ next, children, onClick, title }) => {
  let basicClass = "";
  if (next) {
    basicClass += " primary";
  }
  return (
    <div className="card">
      <div className="section">
        <h3 className="doc">{title}</h3>
        <div />
        <button onClick={onClick} className={basicClass}>
          {children}
        </button>
      </div>
    </div>
  );
};
