/* global grecaptcha */
import React, { useState, useEffect } from "react";
import "./App.css";
import CommitsBrowser from "./CommitsBrowser";
import reserve from "./reserve";
import axios from "axios";
import logo from "./mplus_log.png";
axios.defaults.withCredentials = true;

const domain = process.env.REACT_APP_DOMAIN;
const domainProtocol = process.env.REACT_APP_DOMAIN_PROTOCOL;
const evtSource = new EventSource(process.env.REACT_APP_SERVER_PATH + "/sse", {
  withCredentials: true
});
function App() {
  const [reserveId, setReserveId] = useState(null);
  const [demourl, setDemoUrl] = useState(null);
  const [editorurl, setEditorurl] = useState(null);
  const [sessionExpired, setSessionExpired] = useState(false);
  const [editorOpened, setEditorOpened] = useState(
    localStorage.getItem("editorOpen")
  );
  useEffect(() => {
    new Promise((resolve, reject) => {
      grecaptcha.ready(function() {
        grecaptcha
          .execute(process.env.REACT_APP_CAPTCHA_SITE_KEY, { action: "submit" })
          .then(function(token) {
            console.log("got the captcha token" + token);
            resolve(token);
          });
      });
    })
      .then(token => {
        return axios
          .get(process.env.REACT_APP_SERVER_PATH + "/checkcaptcha", {
            params: { token }
          })
          .then(({ data }) => data);
      })
      .then(_ => reserve())
      .then(trid => {
        setReserveId(trid);
        if (trid) {
          setDemoUrl(domainProtocol + "rn" + trid + "." + domain);
          setEditorurl(domainProtocol + "th" + trid + "." + domain);
        }
      })
      .catch(err => {
        console.log(err);
        alert(
          "All the emulators are occupied, please try again in couple of minutes"
        );
      });
  }, []);

  evtSource.onmessage = ev => {
    console.log(JSON.parse(ev.data));
    if (JSON.parse(ev.data).event === "expiry") {
      localStorage.clear();
      setSessionExpired(true);
    }
  };
  return window["openDatabase"] !== undefined ? (
    sessionExpired ? (
      <div style={{ height: "100vh" }}>
        <div style={{ height: "20vh" }} />
        <div className="card fluid" style={{ margin: "auto", width: "40%" }}>
          <h3 className="doc section">Your session has expired</h3>
          <p className="doc section">
            <button onClick={_ => document.location.reload()}>Reload</button>
          </p>
        </div>
      </div>
    ) : (
      <div>
        <div className="flexContainer">
          <div
            className={
              "textOrCode" + (editorOpened === "true" ? " iframeHolder" : "")
            }
          >
            {reserveId ? (
              editorOpened ? (
                <iframe
                  src={editorurl}
                  title="editor"
                  frameborder="0"
                  allowfullscreen
                  style={{ height: "100%", width: "100%" }}
                />
              ) : (
                <CommitsBrowser reserveId={reserveId} />
              )
            ) : null}
            <div style={{ position: "fixed", bottom: "0px", width: "75%" }}>
              <footer style={{ padding: "5px" }}>
                <a href="https://maximoplus.com">
                  <img
                    src={logo}
                    style={{
                      width: "140px",
                      marginRight: "40px",
                      marginBottom: "-6px"
                    }}
                  />
                </a>

                <button
                  onClick={_ => {
                    if (!editorOpened) {
                      axios
                        .get(
                          process.env.REACT_APP_SERVER_PATH +
                            "/checkoutuserbranch"
                        )
                        .then(({ data }) => {
                          console.log("reserverd branch ");
                          console.log(data);
                          localStorage.setItem("editorOpen", !editorOpened);
                          setEditorOpened(!editorOpened);
                        })
                        .catch(err => {
                          alert("Not able to open editor");
                          console.log(err);
                        });
                      //when opening the editor, first make sure we go to the user branch
                    } else {
                      axios.get(process.env.REACT_APP_SERVER_PATH + "/logs");
                      axios
                        .get(
                          process.env.REACT_APP_SERVER_PATH + "/saveeditorlocal"
                        )
                        .catch(err => {
                          alert("Not able to save changes");
                          return true;
                        })
                        .then(_ => {
                          let commit = localStorage.getItem("githash");
                          axios
                            .get(
                              process.env.REACT_APP_SERVER_PATH +
                                "/gitcheckout/" +
                                commit
                            )
                            .then(_ => {
                              localStorage.setItem("editorOpen", !editorOpened);
                              setEditorOpened(!editorOpened);
                            });
                        });
                    }
                  }}
                >
                  {editorOpened ? "Back to Tutorial" : "Open Editor"}
                </button>
              </footer>
            </div>
          </div>
          <div className="deviceEmulator iframeHolder">
            {demourl ? (
              <iframe
                src={demourl}
                title="simulator"
                frameborder="0"
                allowfullscreen
                allow="camera *;"
                style={{ height: "100%", width: "100%" }}
              />
            ) : null}
          </div>
        </div>
      </div>
    )
  ) : (
    <div style={{ height: "100vh" }}>
      <div style={{ height: "20vh" }} />
      <div className="card fluid" style={{ margin: "auto", width: "40%" }}>
        <h3 className="doc section">Browser not supported</h3>
        <p className="doc section">
          Your browser doesn't support the SQLite database. Maximoplus uses
          SQLite as its internal storage, which is a standard for native
          applications, but was removed from the web standards. To see the demo
          properly use Google Chrome or Chrome compatible browsers - Chromium or
          new Microsoft Edge. Check the following link for more info:{" "}
          <a href="https://caniuse.com/#feat=sql-storage">Supported Browsers</a>
        </p>
      </div>
    </div>
  );
}

export default App;
