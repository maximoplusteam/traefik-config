import React, { useState, useEffect } from "react";
import axios from "axios";
import ReactMarkdown from "react-markdown";
import "github-markdown-css";
import ActionButton from "./ActionButton";
import CodeBlock from "./CodeBlock";

export default reserveId => {
  let currCommit = localStorage.getItem("currentCommit");
  const moveCurrCommit = (x, { commit }) => {
    console.log(x);

    axios
      .get(process.env.REACT_APP_SERVER_PATH + "/gitcheckout/" + commit)
      .then(_ => {
        localStorage.setItem("currentCommit", x.toString());
        localStorage.setItem("githash", commit); //to be used from the main page when coming back from editor
      })
      .catch(err => {
        console.log(err);
        if (
          err.response &&
          err.response.data &&
          "Invalid Session" === err.response.data
        ) {
          document.location.reload();
          return;
        }
        alert("No such commit");
      });
  };
  if (currCommit === null || currCommit === undefined) {
    localStorage.setItem("currentCommit", "0");
    currCommit = 0;
  } else {
    currCommit = parseInt(currCommit);
  }
  const [commits, setCommits] = useState([]);
  const [currentCommit, setCurrentCommit] = useState(currCommit);
  const goPrev = () => {
    if (currentCommit > 0) {
      setCurrentCommit(currentCommit - 1);
      moveCurrCommit(currentCommit - 1, commits[currentCommit - 1]);
    }
  };
  const goNext = () => {
    if (currentCommit < commits.length - 1) {
      setCurrentCommit(currentCommit + 1);
      moveCurrCommit(currentCommit + 1, commits[currentCommit + 1]);
    }
  };

  useEffect(
    () => {
      axios
        .get(process.env.REACT_APP_SERVER_PATH + "/listcommits")
        .then(({ data }) => {
          localStorage.setItem("githash", data[0].commit);
          return setCommits(data);
        });
    },
    [reserveId]
  );
  console.log(commits);
  if (commits.length === 0) return null;
  const { content } = commits[currentCommit];
  const previousTitle =
    commits[currentCommit - 1] && commits[currentCommit - 1].title;
  const nextTitle =
    commits[currentCommit + 1] && commits[currentCommit + 1].title;
  return (
    <div style={{ padding: "30px", height: "90vh", overflowY: "auto" }}>
      <div className="markdown-body">
        <ReactMarkdown source={content} renderers={{ code: CodeBlock }} />
      </div>
      <div className="row" style={{ marginTop: "50px" }}>
        {previousTitle ? (
          <ActionButton onClick={goPrev} title={previousTitle}>
            Previous
          </ActionButton>
        ) : null}
        {nextTitle ? (
          <ActionButton onClick={goNext} next={true} title={nextTitle}>
            Next
          </ActionButton>
        ) : null}
      </div>
    </div>
  );
};
