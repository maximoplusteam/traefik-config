import axios from "axios";
export default () => {
  const trid = localStorage.getItem("trid");
  return axios
    .get(process.env.REACT_APP_SERVER_PATH + "/reservesr", { params: { trid } })
    .then(({ data }) => {
      if (data !== trid) {
        localStorage.setItem("trid", data);
        localStorage.removeItem("editorOpen");
        localStorage.removeItem("githash");
        localStorage.setItem("currentCommit", "0");
        return data;
      } else {
        return trid;
      }
    });
};
