const Docker = require("dockerode");
var ncp = require("ncp").ncp;

const chmodr = require("chmodr");
const { addNewIdToRedis, getNumberOfIds, MAX_IDS } = require("../redis");

ncp.limit = 16;

const sourceVolume = process.env.SOURCE_VOLUME;
const destVolume = process.env.DEST_VOLUME;
const domainName = process.env.DOMAIN_NAME;
const dockerNetwork = process.env.DOCKER_NETWORK;

console.log(sourceVolume);
console.log(destVolume);

const randHex = function(len) {
  var maxlen = 8,
    min = Math.pow(16, Math.min(len, maxlen) - 1),
    max = Math.pow(16, Math.min(len, maxlen)) - 1,
    n = Math.floor(Math.random() * (max - min + 1)) + min,
    r = n.toString(16);
  while (r.length < len) {
    r = r + randHex(len - maxlen);
  }
  return r;
};

const docker = new Docker();

const createEnvironment = () => {
  const id = randHex(10);
  const routerId = "traefik.http.routers.rn" + id + ".rule";
  const hostString = "Host(`rn" + id + "." + domainName + "`)";

  const Labels = {};
  Labels[routerId] = hostString;


  return new Promise((resolve, reject) =>
    ncp(sourceVolume, destVolume + "/" + id, err => {
      if (err) {
        reject(err);
      } else {
        resolve(destVolume + "/" + id);
      }
    })
  )
    .then(destVol => {
      return new Promise((resolve, reject) => {
        chmodr(destVol, 0o775, err => {
          if (err) {
            console.log(err);

            reject(err);
          } else {
            console.log("chmod succ");
            resolve(destVol);
          }
        });
      });
    })
    .then(destVol => {
      console.log("create the container");
      return docker.createContainer({
        Image: "maximoplus/rndemo",
        name: "rn" + id,
        Labels,
        HostConfig: {
          Binds: [destVolume + "/" + id + "/src:/rndemo/src"],
          NetworkMode: dockerNetwork
        }
      });
    })
    .then(container => {
      console.log("start the container");
      return container.start();
    })
    .then(_ => {
      const Labels = {};
      const routerId = "traefik.http.routers.th" + id + ".rule";
      const hostString = "Host(`th" + id + "." + domainName + "`)";
      //        const routerMiddlewareLabel =
      //          "traefik.http.routers.th" + id + ".middlewares";
      //        const routerMiddlewareValue = "test-auth@docker";
      Labels[routerId] = hostString;
      //        Labels[authLabel] = authServer;
      //        Labels[authLabel2] = "false";
      //      Labels[routerMiddlewareLabel] = routerMiddlewareValue;
      return docker.createContainer({
        Image: "maximoplus/theia",
        name: "th" + id,
        Labels,
        HostConfig: {
          Binds: [destVolume + "/" + id + "/src:/home/project"],
          NetworkMode: dockerNetwork
        }
      });
    })
    .then(container => {
      return container.start();
    })
    .then(_ => {
      return id;
    });
};

const createOne = () => {
  return getNumberOfIds().then(cnt => {
    if (cnt + 1 > MAX_IDS) {
      return Promise.reject("Maximum number of servers");
    }
    return createEnvironment().then(id => {
      console.log("created the environment " + id);
      return addNewIdToRedis(id);
    });
  });
};

const createServers = async noOfServers => {
  const cnt = await getNumberOfIds();
  const nsrvs = parseInt(noOfServers);
  if (parseInt(cnt) + parseInt(noOfServers) > parseInt(MAX_IDS)) {
    return Promise.reject("Maximum number of servers");
  }
  const servers = [];
  for (let i = 0; i < nsrvs; i++) {
    const id = await createOne();
    console.log("awaited:" + id);
    servers.push(id);
  }
  return servers;
};

module.exports = { createOne, createServers };
