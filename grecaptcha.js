const fetch = require("isomorphic-fetch");

const CAPTCHA_SERVER_KEY = process.env.CAPTCHA_SERVER_KEY;

const checkCaptcha = async (token, ip) => {
  const url = `https://www.google.com/recaptcha/api/siteverify?secret=${CAPTCHA_SERVER_KEY}&response=${token}&remoteip=${ip}`;
  const response = await fetch(url, { method: "post" });
  const res = await response.json();
  console.log(res);
  const { success, score } = res;
  if (success == true && score > 0.5) {
    return true;
  }
  return false;
};

module.exports = { checkCaptcha };
