const NRP = require("node-redis-pubsub");
const config = {
  port: 6379,
  host: process.env.REDIS_HOST,
  scope: "sessions"
};

const nrp = new NRP(config);

const subscribe = f => {
  nrp.on("sessions", data => f(data));
};

const send = data => {
  nrp.emit("sessions", data);
};

module.exports = { send, subscribe };
