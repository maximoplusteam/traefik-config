const redis = require("redis");
const { send, subscribe } = require("./pubsub");

const redisHost = process.env.REDIS_HOST; //redisHost should be in Docker network, without exposing the port to the OS
const redisPort = 6379;
const redisClient = redis.createClient(redisPort, redisHost);
const { promisify } = require("util");
const MAX_IDS = 20;

const lpush = promisify(redisClient.lpush).bind(redisClient);
const lpop = promisify(redisClient.lpop).bind(redisClient);
const llen = promisify(redisClient.llen).bind(redisClient);
const set = promisify(redisClient.set).bind(redisClient);
const get = promisify(redisClient.get).bind(redisClient);
const del = promisify(redisClient.del).bind(redisClient);
const sunionstore = promisify(redisClient.sunionstore).bind(redisClient);
const lrange = promisify(redisClient.lrange).bind(redisClient);

const addNewIdToRedis = async id => {
  //id is a constant returned from the previous function
  await lpush("all_ids", id);
  await lpush("available_ids", id);
  //  redisClient.set("ser" + id, new Date().toString()); - this should be put only when reserve container
  console.log("redis and " + id);
  return id;
};

const reserveOne = async _id => {
  console.log("sending id=" + _id);
  const resvd = await isReserved(_id);
  if (resvd) {
    console.log("got is reserved!" + resvd);
    await confirmReservation(_id);
    return _id;
  }
  //  console.log("geting the available id");
  const id = await lpop("available_ids");
  //  console.log("got from redis" + id);
  if (!id) {
    throw new Error(
      "All the emulators are occupied, please try again in couple of minutes"
    );
  }
  await set("ser-" + id, Date.now());
  await setAccessTiming("access-", id);
  return id;
};

const confirmReservation = async id => {
  //refresh the time of the variable, so the isntance doesn't deactivate by background job
  await set("ser-" + id, new Date());
  return id;
};

const isValidId = async id => await get("ser-" + id);

const getNumberOfIds = async () => {
  return await llen("all_ids");
};

const removeReservation = async id => {
  await lpush("available_ids", id);
  return await del("ser-" + id);
};

const isReserved = async id => {
  if (!id) {
    return Promise.resolve(false);
  }
  return await get("ser-" + id);
};

const releaseAllIds = async () => {
  await del("available_ids");
  const allIds = await lrange("all_ids", 0, -1);
  for (const currId of allIds) {
    await del("ser-" + currId);
    await del("access-" + currId);
    await del("change-" + currId);
    await lpush("available_ids", currId);
  }

  return allIds;
};

const getAllOccupiedIds = async () => {
  const allIds = await lrange("all_ids", 0, -1);
  const availableIds = await lrange("available_ids", 0, -1);
  const freeIds = [];
  for (const id of allIds) {
    if (!availableIds.some(aid => aid === id)) {
      freeIds.push(id);
    }
  }
  return freeIds;
};

const setAccessTiming = async (prefix, id) => {
  return await set(prefix + id, Date.now());
};

const removeId = async id => {
  await del("ser-" + id);
  await del("access-" + id);
  await del("change-" + id);
  await lpush("available_ids", id);
  send({ id, timeToExpiry: 0 });
};

const removeIds = async ids => {
  for (const id of ids) {
    await removeId(id);
  }
  return ids;
};

const getExpiredIds = async expiryPeriod => {
  //the expired ids are going to be deleted, and the git reverted to the first stage
  const occupiedIds = await getAllOccupiedIds();
  const expiredIds = [];

  for (const oid of occupiedIds) {
    const accessDate = await get("access-" + oid);
    const changeDate = await get("change-" + oid);
    if (accessDate && Date.now() - parseInt(accessDate) > expiryPeriod) {
      expiredIds.push(oid);
    } else if (changeDate && Date.now() - parseInt(changeDate) > expiryPeriod) {
      expiredIds.push(oid);
    }
  }
  return expiredIds;
};

const getFreeIds = async () => {
  return await lrange("available_ids", 0, -1);
};

module.exports = {
  MAX_IDS,
  addNewIdToRedis,
  reserveOne,
  getNumberOfIds,
  removeReservation,
  redisClient,
  confirmReservation,
  isReserved,
  releaseAllIds,
  getAllOccupiedIds,
  setAccessTiming,
  getExpiredIds,
  removeIds,
  isValidId,
  subscribe,
  removeId,
  getFreeIds
};
