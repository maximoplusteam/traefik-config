In this step, we are adding the __List__ component to our application. Our template has predefined start screens, located in the __screens__ directory. MaximoPlus template uses the most popular navigation library for React Native - [React Navigation](https://reactnavigation.org/). If you are interested in finding out how it works, check out the template source code.

Our changes are  in ListScreen.js:

```jsx
export default () => (
  <MaxList
    listTemplate="po"
    container="pocont"
    label="PO List"
    columns={["ponum", "description", "status"]}
    norows={20}
    initdata={true}
    navigate={navigation => navigation.navigate("Details")}
  />
);
```

The __container__ property tells the container to which our List connects. We need to specify beforehand what columns we are using, with the __columns__ attribute. The __norows__ gives the initial number of rows in the List. When the user reaches the end of the List, it fetches more data from the server or offline database. The __initdata__ property tells do you want the data brought automatically (basically always should be set to _true_). __label__ displays text in the header of the current tab. 

The __listTemplate__ defines how List draws each row on the screen. It is basically just a plain React component, defined in the __components/listTemplates.js__ . 

When the user taps on the item in the List component, he wants to see the details. The __navigate__ property specifies where the application navigates at that time. In our case, it navigates to the _Details_ tab.

The _Details_ tab is still empty, proceed to the next step is to see how we fill it with the _Section__ component.
