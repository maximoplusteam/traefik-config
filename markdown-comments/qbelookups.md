To make the search easier, we need to define the lookups for the search form (QBE Section).
To define it, we use the same steps we used for the ordinary Section; we add the field metadata:

```jsx
export default () => (
  <QbeSection
    container="pocont"
    columns={["ponum", "description", "status", "shipvia"]}
    navigate={navigation => navigation.navigate("List")}
    label="Search POs"
    metadata={{
      SHIPVIA: {
        hasLookup: true,
        listTemplate: "qbevaluelist",
        offlineReturnColumn: "VALUE"
      },
      STATUS: {
        hasLookup: true,
        listTemplate: "qbevaluelist",
        offlineReturnColumn: "VALUE"
      }
    }}
  />
);
```

The only difference is in the list template. Qbe sections can select multiple values from the value lists, and we need an indicator for that in the template itself. Check it out in the emulator window now to better understand the difference.
