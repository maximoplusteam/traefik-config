The __Qbe Section__ component performs the search in Maximo, in the same manner, the _Advanced Search_ in Maximo does.

Below is the content of _screens/QbeSectionScreen.js_ , where we defined our search component. The properties are the same as for the Section component, except for the __navigate__.
The component uses _navigate_ property to navigate to one of the tabs after the search. Usually, that is the tab with the _List_ component.

```jsx
export default () => (
  <QbeSection
    container="pocont"
    columns={["ponum", "description", "status", "shipvia"]}
    navigate={navigation => navigation.navigate("List")}
    label="Search POs"
  />
);
```

Note that in offline mode, the search is case sensitive. 
To see how to add the lookups to your application, proceed to the next example.

