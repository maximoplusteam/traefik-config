In this interactive demo, we demonstrate the ease of creating the MaximoPlus application. 

MaximoPlus is a platform for building native mobile applications using React Native. The demo application will run in the browser frame on the right side, using React Native for the web.
It will look almost identical as a real native application, but keep in mind that it may have some differences in look and feel and behavior compared to the native app. 

Our application will run in offline(disconnected) mode, and for that, MaximoPlus requires the SQLite database.
The only browsers supporting that are Chrome and Chrome compatible browsers(new Edge or Chromium).

At this stage, we have just the skeleton of an app, a template. There are no MaximoPlus components yet.
Feel free to skip to the next step, or examine or change the template by pressing the __Open Editor__ button.


