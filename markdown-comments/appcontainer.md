The first step in creating the app is defining the application container, which sends data and commands to and from the MaximoPlus server. 

This demo runs in the offline(disconnected) mode, connected to the offline database instead of the MaximoPlus server.
When online, the application container is bound to the main Mbo of the application. In our template, we define them in the __src/Containers.js__ file.
For example:

```jsx
export default props => (
  <>
    <AppContainer id="pocont" mboname="po" appname="po" offlineenabled={true} />
  </>
);
```

The code snippet above defines the container on the __po__ application, with the __po__ Mbo, and enables the offline mode.
You don't see any changes in this step. Please proceed to the next step to see how we add the visual components to our application.

