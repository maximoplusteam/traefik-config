MaximoPlus gives full access to smartphone features. There are many predefined integrations available; however, it is easy to create your own if you miss some features. Checkout [Integrating Maps into your application](https://maximoplus.com/blog/beyound-the-template/) for the more complex example. 

The simplest possible case of smartphone usage is making phone calls. If we want our app to dial a number contained in a field, we need to add the metadata:

```jsx
export default () => {
  return (
    <Section
      container="pocont"
      options={{
        headerTitle: "PO Details"
      }}
      columns={[
        "ponum",
        "description",
        "status",
        "shipvia",
        "orderdate",
        "vendor",
        "vendor.phone"
      ]}
      metadata={{
        SHIPVIA: {
          hasLookup: true,
          listTemplate: "valuelist",
          offlineReturnColumn: "VALUE"
        },
        "VENDOR.PHONE": { phonenum: true }
      }}
    />
  );
};

```

You can use the same metadata mechanism to create your integrations. For example, sending the SMS instead of making the phone calls. Check out the documentation for more details.
