To see our section component in action, click on the _Details_ tab or tap on the List item.

A Section is simply a form bound to the data of the current Mbo in Maximo. First, we have to define the container and the columns displayed in Section, the same as we had done for the List component.
The labels, data types, and other column info comes from Maximo itself. Our offline database stores the data and editability info in the database.

```jsx
export default () => {
  return (
    <Section
      container="pocont"
      options={{ headerTitle: "PO Details" }}
      columns={[
        "ponum",
        "description",
        "status",
        "shipvia",
        "orderdate",
        "vendor",
        "vendor.phone"
      ]}
  />
  );
};

```

You may try to edit the data in the form. Try to find some PO with the _WAPPR_ status and change its data.
Follow the next step to see the search component (QBE Section) in action.
