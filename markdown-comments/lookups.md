In MaximoPlus, a lookup is the List of values (the domain in Maximo language) attached to a column. The same domain rules existing in Maximo are also correct for MaximoPlus.
Before adding the lookup to your column, the domain data(static or dynamic) must exist in Maximo.
For our example, we used the _SHIPVIA_ column of our Section:

```jsx
export default () => {
  return (
    <Section
      container="pocont"
      options={{ headerTitle: "PO Details" }}
      columns={[
        "ponum",
        "description",
        "status",
        "shipvia",
        "orderdate",
        "vendor",
        "vendor.phone"
      ]}
      metadata={{
        SHIPVIA: {
          hasLookup: true,
          listTemplate: "valuelist",
          offlineReturnColumn: "VALUE"
        }
      }}
    />
  );
};
```

The __metadata__ property adds information about the columns. We used it to say that the _SHIPVIA_ has lookup (__hasLookup__ attribute), to define how the lookup will display ( __listTemplate__), and to specify which column should return from the List in offline mode(__offlineReturnColumn__). Note that the last property is mandatory if you want to use the application in the offline mode. While online, it is handled automatically by Maximo embedded in MaximoPlus server.

The _metadata_ property is generic, meaning you can use it to pass the columns' additional data. That may be handy if you want to make your components or change the behavior of the existing ones (see [Custom Component doc](https://maximoplus.com/manual/html/#/README?id=custom-integrations) ).
