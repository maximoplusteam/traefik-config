For a bit more complex example, we will demonstrate how the photo upload works. It is just marginally more complicated because we need to define how the photo upload is open. For our example, we use the standard React Navigation action button and open the camera dialog with the __openPhotoUpload__ utility function:

```jsx
export default () => {
  return (
    <Section
      container="pocont"
      options={{
        headerTitle: "PO Details",
        headerRight: () => (
          <Button
            onPress={() => openPhotoUpload("po")}
            title="Photo"
            color="#fff"
            type="clear"
            style={{ marginRight: 5 }}
          />
        )
      }}
      columns={[
        "ponum",
        "description",
        "status",
        "shipvia",
        "orderdate",
        "vendor",
        "vendor.phone"
      ]}
      metadata={{
        SHIPVIA: {
          hasLookup: true,
          listTemplate: "valuelist",
          offlineReturnColumn: "VALUE"
        },
        "VENDOR.PHONE": { phonenum: true }
      }}
    />
  );
}
```

Notice that you need to pass the id of the container to the photo upload function. Keep in mind, we are running demo in the offline(disconnected) mode, and don't save the photo anywhere (you can easily add the offline photo functionality if required).

As already said, browser emulator is limited in functionality.  We can't cover more advanced integrations (like barcode scanning or Maps integration), simply because browser APIs don't support them.

If you want to see it in real action, install Expo on your phone, MaximoPlus on your computer, and follow along with our blog post series [Getting started with MaximoPlus](https://maximoplus.com/blog/getting-started-with-maximoplus/)
